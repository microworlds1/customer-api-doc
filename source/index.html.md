---
title: Stillio API Reference

language_tabs:
  - json

toc_footers:
  - <a href='https://support.stillio.com/article/18-use-stillio-api-to-fetch-screenshots-and-urls'>Sign Up for a Developer API token</a>
  - <a href='https://github.com/slatedocs/slate'>Documentation Powered by Slate</a>

search: true

code_clipboard: true
---

Stillio API (version 3)
==============================

You can use the Stillio API for interacting with the webpages and screenshots stored on the Stillio platform. This reference outlines version 3 of the API.

# About the API

* The API uses pragmatic REST as its architectural style
* Requests use [JSON-LD 1.0](https://www.w3.org/TR/json-ld/) as method for serializing data
* The API uses shared, established vocabularies for modelling data: [Hydra](http://www.hydra-cg.com/spec/latest/core/) for collections and errors and [Schema.org](https://schema.org/) for webpages and screenshots. For specific properties Stillio uses its own vocabulary
* The API may change. For example, new properties may be added to the headers or body of endpoints

# Authentication
The API uses a Bearer token for authentication. You can create your token in the [Stillio app](https://support.stillio.com/article/18-use-stillio-api-to-fetch-screenshots-and-urls). Do not share the token with untrusted parties or use it directly in your browser client.

# Errors

The API uses common HTTP status codes: `2xx` for success, `4xx` for errors caused by the caller and `5xx` for server errors. The table underneath lists the most common errors that can occur:

Status | Description
--|--
`400` | The request is invalid
`401` | The Bearer token is invalid
`404` | The resource does not exist
`415` | The content type is not supported
`422` | The resource cannot be processed
`500` | A server error has occurred
`503` | The server is under maintenance

#### Response

> Example body

```json
{
    "type": "Error",
    "title": "Bad Request",
    "description": "..."
}
```

##### Headers
Name | Value
--|--
Status | HTTP status, e.g. `400 Bad Request`, `404 Not Found`, `500 Internal Server Error`
Content-Type | `application/json`
Link | `<https://api.stillio.com/v3/contexts>; rel="http://www.w3.org/ns/json-ld#context"; type="application/ld+json"`

# Endpoints

## Webpages

### Get webpages

#### Request

> Request endpoint

```
GET https://api.stillio.com/v3/webpages?page={page}&limit={limit}&sort={sort}&order={order}`
```

> Example requests

```
GET https://api.stillio.com/v3/webpages

GET https://api.stillio.com/v3/webpages?page=2

GET https://api.stillio.com/v3/webpages?page=2&limit=5

GET https://api.stillio.com/v3/webpages?sort=dateCreated&order=desc
```

##### Headers
Name | Value
--|--
Authorization | `Bearer: {token}`
Accept | `application/json`

##### Query string parameters
Property | Data type | Required | Description
--|--|--|--
`page` | Number | No | Page index. Default: 1
`limit` | Number between 1 and 100 | No | Number of items per page. Default: 50
`sort` | Text: `dateCreated`, `isPartOf.name`, `name`, `url` | No | Field to sort the items by. Default: `isPartOf.name`
`order` | Text: `asc`, `desc` | No | Sorting direction of the items: ascending or descending. Default: `asc`

#### Response

##### Headers
Name | Value
--|--
Status | `200 OK`
Content-Type | `application/json`
Link | `<https://api.stillio.com/v3/contexts>; rel="http://www.w3.org/ns/json-ld#context"; type="application/ld+json"`

> Sample response body

```json
{
    "type": "Collection",
    "id": "https://api.stillio.com/v3/webpages",
    "totalItems": 123,
    "members": [
        {
            "type": "WebPage",
            "id": "https://api.stillio.com/v3/webpages/{webpageId}",
            "identifier": "{webpageId}",
            "name": "Some page",
            "url": "http://www.example.org/somePage.html?q=a",
            "keywords": ["Example1", "Example2"],
            "dateCreated": "2019-01-29T11:24:58.743Z",
            "dateModified": "2019-01-29T11:24:58.743Z",
            "subjectOf": {
                "type": "Collection",
                "id": "https://api.stillio.com/v3/webpages/{webpageId}/screenshots"
            },
            "isPartOf": {
                "type": "WebPageGroup",
                "name": "example.org"
            }
        },
        // ...
    ],
    "view": {
        "type": "PartialCollectionView",
        "id": "https://api.stillio.com/v3/webpages?page=3",
        "first": "https://api.stillio.com/v3/webpages?page=1",
        "previous": "https://api.stillio.com/v3/webpages?page=2",
        "next": "https://api.stillio.com/v3/webpages?page=4",
        "last": "https://api.stillio.com/v3/webpages?page=15"
    }
}
```

##### Body
Property | Data type | Description
--|--|--
`type` | Text | Type of the collection: `Collection`
`id` | URI | URL of the collection
`totalItems` | Number | Number of webpage records in the collection
`members` | List | List of webpage records
`members[].type` | Text | Type of the webpage record: `WebPage`
`members[].id` | URI | URL of the webpage record
`members[].identifier` | Text | Unique identifier of the webpage record
`members[].name` | Text | Name of the webpage record
`members[].url` | URI | URL of the webpage
`members[].keywords` | List | List of keywords or tags to describe the webpage record
`members[].dateCreated` | Date, in ISO 8601 date format | Date and time at which the webpage record was created (UTC)
`members[].dateModified` | Date, in ISO 8601 date format | Date and time at which the webpage record was modified (UTC)
`members[].subjectOf` | Resource | Reference to the collection of screenshots of this webpage record
`members[].subjectOf.type` | Text | Type of the screenshot collection: `Collection`
`members[].subjectOf.id` | URI | URL of the screenshot collection
`members[].isPartOf` | Resource | The webpage group the webpage record belongs to
`members[].isPartOf.type` | Text | Type of the webpage group record: `WebPageGroup`
`members[].isPartOf.name` | Text | Name of the webpage group record. This name is derived from the URL of the webpage
`view` | Resource | Controls for paginating the collection
`view.type` | Text | Type of the resource: `PartialCollectionView`
`view.id` | URI | URL of the current page in the collection
`view.first` | URI | URL of the first page in the collection. Not set if there is no first page
`view.previous` | URI | URL of the previous page in the collection. Not set if there is no previous page
`view.next` | URI | URL of the next page in the collection. Not set if there is no next page
`view.last` | URI | URL of the last page in the collection. Not set if there is no last page

### Get a webpage

#### Request

> Request endpoint

```
GET https://api.stillio.com/v3/webpages/{webpageId}
```

##### Headers
Name | Value
--|--
Authorization | `Bearer: {token}`
Accept | `application/json`

#### Response

##### Headers
Name | Value
--|--
Status | `200 OK`
Content-Type | `application/json`
Link | `<https://api.stillio.com/v3/contexts>; rel="http://www.w3.org/ns/json-ld#context"; type="application/ld+json"`

> Sample response body

```json
{
    "type": "WebPage",
    "id": "https://api.stillio.com/v3/webpages/{webpageId}",
    "identifier": "{webpageId}",
    "name": "Some page",
    "url": "http://www.example.org/somePage.html?q=a",
    "keywords": ["Example1", "Example2"],
    "dateCreated": "2019-01-29T11:24:58.743Z",
    "dateModified": "2019-01-29T11:24:58.743Z",
    "subjectOf": {
        "type": "Collection",
        "id": "https://api.stillio.com/v3/webpages/{webpageId}/screenshots"
    },
    "isPartOf": {
        "type": "WebPageGroup",
        "name": "example.org"
    },
    "collection": "https://api.stillio.com/v3/webpages"
}
```

##### Body
Property | Data type | Description
--|--|--
`type` | Text | Type of the webpage record: `WebPage`
`id` | URI | URL of the webpage record
`identifier` | Text | Unique identifier of the webpage record
`name` | Text | Name of the webpage record
`url` | URI | URL of the webpage
`keywords` | List | List of keywords or tags to describe the webpage record
`dateCreated` | Date, in ISO 8601 date format | Date and time at which the webpage record was created (UTC)
`dateModified` | Date, in ISO 8601 date format | Date and time at which the webpage record was modified (UTC)
`subjectOf` | Resource | Reference to the collection of screenshots of this webpage record
`subjectOf.type` | Text | Type of the screenshot collection: `Collection`
`subjectOf.id` | URI | URL of the screenshot collection
`isPartOf` | Resource | The webpage group the webpage record belongs to
`isPartOf.type` | Text | Type of the webpage group record: `WebPageGroup`
`isPartOf.name` | Text | Name of the webpage group record. This name is derived from the URL of the webpage
`collection` | URI | URL of the collection to which this webpage record belongs

## Screenshots

### Get screenshots of a webpage

#### Request

> Request endpoint

```
GET https://api.stillio.com/v3/webpages/{webpageId}/screenshots?page={page}&limit={limit}&sort={sort}&order={order}
``` 

> Example requests

```
GET https://api.stillio.com/v3/webpages/{webpageId}/screenshots

GET https://api.stillio.com/v3/webpages/{webpageId}/screenshots?page=2

GET https://api.stillio.com/v3/webpages/{webpageId}/screenshots?page=2&limit=5

GET https://api.stillio.com/v3/webpages/{webpageId}/screenshots?sort=dateCreated&order=desc
```

##### Headers
Name | Value
--|--
Authorization | `Bearer: {token}`
Accept | `application/json`

##### Query string parameters
Property | Data type | Required | Description
--|--|--|--
`page` | Number | No | Page index. Default: 1
`limit` | Number between 1 and 100 | No | Number of items per page. Default: 50
`sort` | Text: `dateCreated` | No | Field to sort the items by. Default: `dateCreated`
`order` | Text: `asc`, `desc` | No | Sorting direction of the items: ascending or descending. Default: `desc`

#### Response

##### Headers
Name | Value
--|--
Status | `200 OK`
Content-Type | `application/json`
Link | `<https://api.stillio.com/v3/contexts>; rel="http://www.w3.org/ns/json-ld#context"; type="application/ld+json"`

> Sample response body

```json
{
    "type": "Collection",
    "id": "https://api.stillio.com/v3/webpages/{webpageId}/screenshots",
    "totalItems": 123,
    "members": [
        {
            "type": "ImageObject",
            "id": "https://api.stillio.com/v3/webpages/{webpageId}/screenshots/{screenshotId}",
            "identifier": "{screenshotId}",
            "dateCreated": "2019-01-29T11:23:58.624Z",
            "dateModified": "2019-01-29T11:24:58.743Z",
            "contentUrl": "https://stillio-hosting/path/to/image.png",
            "encodingFormat": "image/png",
            "thumbnail": {
                "type": "ImageObject",
                "contentUrl": "https://stillio-hosting/path/to/thumbnail.png",
                "encodingFormat": "image/png"
            },
            "about": {
                "type": "WebPage",
                "id": "https://api.stillio.com/v3/webpages/{webpageId}"
            }
        },
        // ...
    ],
    "view": {
        "type": "PartialCollectionView",
        "id": "https://api.stillio.com/v3/webpages/{webpageId}/screenshots?page=3",
        "first": "https://api.stillio.com/v3/webpages/{webpageId}/screenshots?page=1",
        "previous": "https://api.stillio.com/v3/webpages/{webpageId}/screenshots?page=2",
        "next": "https://api.stillio.com/v3/webpages/{webpageId}/screenshots?page=4",
        "last": "https://api.stillio.com/v3/webpages/{webpageId}/screenshots?page=15"
    }
}
```

##### Body
Property | Data type | Description
--|--|--
`type` | Text | Type of the collection: `Collection`
`id` | URI | URL of the collection
`totalItems` | Number | Number of screenshot records in the collection
`members` | List | List of screenshot records
`members[].type` | Text | Type of the screenshot record: `ImageObject`
`members[].id` | URI | URL of the screenshot record
`members[].identifier` | Text | Unique identifier of the screenshot record
`members[].dateCreated` | Date, in ISO 8601 date format | Date and time at which the screenshot record was created (UTC)
`members[].dateModified` | Date, in ISO 8601 date format | Date and time at which the screenshot record was modified (UTC)
`members[].contentUrl` | URI | URL of the screenshot image
`members[].encodingFormat` | Text, in MIME format | Media type of the screenshot image, e.g. `image/png`
`members[].thumbnail` | Resource | The thumbnail version of the screenshot image
`members[].thumbnail.type` | Text | Type of the thumbnail image: `ImageObject`
`members[].thumbnail.contentUrl` | URI | URL of the thumbnail image
`members[].thumbnail.encodingFormat` | Text, in MIME format | Media type of the thumbnail image, e.g. `image/png`
`members[].about` | Resource | The webpage record the screenshot record is about
`members[].about.type` | Text | Type of the webpage record: `WebPage`
`members[].about.id` | URI | URL of the webpage record
`view` | Resource | Controls for paginating the collection
`view.type` | Text | Type of the resource: `PartialCollectionView`
`view.id` | URI | URL of the current page in the collection
`view.first` | URI | URL of the first page in the collection. Not set if there is no first page
`view.previous` | URI | URL of the previous page in the collection. Not set if there is no previous page
`view.next` | URI | URL of the next page in the collection. Not set if there is no next page
`view.last` | URI | URL of the last page in the collection. Not set if there is no last page

### Get a screenshot

#### Request

> Request endpoint

```
GET https://api.stillio.com/v3/webpages/{webpageId}/screenshots/{screenshotId}
``` 

##### Headers
Name | Value
--|--
Authorization | `Bearer: {token}`
Accept | `application/json`

#### Response

##### Headers
Name | Value
--|--
Status | `200 OK`
Content-Type | `application/json`
Link | `<https://api.stillio.com/v3/contexts>; rel="http://www.w3.org/ns/json-ld#context"; type="application/ld+json"`

> Sample response body

```json
{
    "type": "ImageObject",
    "id": "https://api.stillio.com/v3/webpages/{webpageId}/screenshots/{screenshotId}",
    "identifier": "{screenshotId}",
    "dateCreated": "2019-01-29T11:23:58.624Z",
    "dateModified": "2019-01-29T11:24:58.743Z",
    "contentUrl": "https://stillio-hosting/path/to/image.png",
    "encodingFormat": "image/png",
    "thumbnail": {
        "type": "ImageObject",
        "contentUrl": "https://stillio-hosting/path/to/thumbnail.png",
        "encodingFormat": "image/png"
    },
    "about": {
        "type": "WebPage",
        "id": "https://api.stillio.com/v3/webpages/{webpageId}"
    },
    "collection": "https://api.stillio.com/v3/webpages/{webpageId}/screenshots"
}
```

##### Body
Property | Data type | Description
--|--|--
`type` | Text | Type of the screenshot record: `ImageObject`
`id` | URI | URL of the screenshot record
`identifier` | Text | Unique identifier of the screenshot record
`dateCreated` | Date, in ISO 8601 date format | Date and time at which the screenshot record was created (UTC)
`dateModified` | Date, in ISO 8601 date format | Date and time at which the screenshot record was modified (UTC)
`contentUrl` | URI | URL of the screenshot image
`encodingFormat` | Text, in MIME format | Media type of the screenshot image, e.g. `image/png`
`thumbnail` | Resource | The thumbnail version of the screenshot image
`thumbnail.type` | Text | Type of the thumbnail image: `ImageObject`
`thumbnail.contentUrl` | URI | URL of the thumbnail image
`thumbnail.encodingFormat` | Text, in MIME format | Media type of the thumbnail image, e.g. `image/png`
`about` | Resource | The webpage record the screenshot record is about
`about.type` | Text | Type of the webpage record: `WebPage`
`about.id` | URI | URL of the webpage record
`collection` | URI | URL of the collection to which this screenshot record belongs

## General information

### Get API context information

This endpoint returns contextual information about the API and does not require authentication. You usually only need this information if your application uses [Linked Data](https://en.wikipedia.org/wiki/Linked_data).

#### Request

> Request endpoint

```
GET https://api.stillio.com/v3/contexts
``` 

##### Headers
Name | Value
--|--
Accept | `application/json`

#### Response

##### Headers
Name | Value
--|--
Status | `200 OK`
Content-Type | `application/ld+json`

> Sample response body

```json
{
    "@context": {
        "type": "@type",
        "id": "@id",
        "as": "https://www.w3.org/ns/activitystreams#",
        "hydra": "http://www.w3.org/ns/hydra/core#",
        "schema": "https://schema.org/",
        "stillio": "https://api.stillio.com/v3/vocab#",
        "ImageObject": "schema:ImageObject",
        "WebPage": "schema:WebPage",
        "WebPageGroup": "stillio:WebPageGroup",
        "IpAddress": "stillio:IpAddress",
        "Collection": "hydra:Collection",
        "PartialCollectionView": "hydra:PartialCollectionView",
        "Error": "hydra:Error",
        "Create": "as:Create",
        "Reject": "as:Reject",
        "summary": {
            "@id": "as:summary"
        },
        "actor": {
            "@id": "as:actor",
            "@type": "@id"
        },
        "object": {
            "@id": "as:object"
        },
        "published": {
            "@id": "as:published",
            "@type": "http://www.w3.org/2001/XMLSchema#dateTime"
        },
        "title": {
            "@id": "hydra:title"
        },
        "description": {
            "@id": "hydra:description"
        },
        "collection": {
            "@id": "hydra:collection",
            "@type": "@id"
        },
        "totalItems": {
            "@id": "hydra:totalItems"
        },
        "members": {
            "@id": "hydra:member"
        },
        "view": {
            "@id": "hydra:view",
            "@type": "@id"
        },
        "first": {
            "@id": "hydra:first",
            "@type": "@id"
        },
        "previous": {
            "@id": "hydra:previous",
            "@type": "@id"
        },
        "next": {
            "@id": "hydra:next",
            "@type": "@id"
        },
        "last": {
            "@id": "hydra:last",
            "@type": "@id"
        },
        "dateCreated": {
            "@id": "schema:dateCreated",
            "@type": "http://www.w3.org/2001/XMLSchema#dateTime"
        },
        "dateModified": {
            "@id": "schema:dateModified",
            "@type": "http://www.w3.org/2001/XMLSchema#dateTime"
        },
        "datePublished": {
            "@id": "schema:datePublished",
            "@type": "http://www.w3.org/2001/XMLSchema#dateTime"
        },
        "identifier": {
            "@id": "schema:identifier"
        },
        "name": {
            "@id": "schema:name"
        },
        "about": {
            "@id": "schema:about"
        },
        "subjectOf": {
            "@id": "schema:subjectOf"
        },
        "isPartOf": {
            "@id": "schema:isPartOf"
        },
        "contentUrl": {
            "@id": "schema:contentUrl",
            "@type": "@id"
        },
        "encodingFormat": {
            "@id": "schema:encodingFormat"
        },
        "url": {
            "@id": "schema:url",
            "@type": "@id"
        },
        "thumbnail": {
            "@id": "schema:thumbnail"
        },
        "isBasedOn": {
            "@id": "schema:isBasedOn",
            "@type": "@id"
        }
    }
}
```
